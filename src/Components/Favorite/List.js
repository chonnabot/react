import React, { Component } from 'react'
import { List } from 'antd'
import ItemFavorite from './Item'


class ListFavorite extends Component {
    render() {
        return (
            <div>
                <List
                    grid={{ gutter: 16, Column: 4 }}
                    dataSource={this.props.items}
                    renderItem={item => (
                        <List.item>
                            <ItemFavorite
                                item={item}
                                onItemMovieClick={this.props.onItemMovieClick}
                            />
                        </List.item>
                    )}
                />
            </div>
        );
    }
}
export default ListFavorite