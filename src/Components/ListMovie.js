import React from 'react'
import { List } from 'antd';
import ItemMovie from './ItemMovie';

function onItemMovieClick(item) {
    console.log(`click item: ${item.title}`)
}

function ListMovie(props) {

    return (
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={props.items}
            renderItem={item => (
                <List.Item>
                    <ItemMovie item={item} onItemMovieClick={props.onItemMovieClick} />
                </List.Item>
            )}
        />
    )
}

export default ListMovie