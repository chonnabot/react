import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu } from 'antd';
import RouteManu from './RouteMenu'

const { Header, Content, Footer } = Layout;
const menus = ['movies', 'favorite', 'profile'];

class Main extends Component {

    state = {
        items: [],
        isShowModel: false,
        itemMovie: null,
        pathName: menus[0]
    }
    onItemMovieClick = (item) => {
        this.setState({ isShowModel: true, itemMovie: item })
    }

    onclickOk = () => {
        this.setState({ isShowModel: false })
    }
    onclickCancel = () => {
        this.setState({ isShowModel: false })
    }

    componentDidMount() {
        const { pathname }= this.props.location;
        var pathName = menus[0];
        if (pathname != '/'){
            pathName = pathname.replace('/','');
            if (!menus.includes(pathName))pathName=menus[0];
        }
        fetch('http://workshopup.herokuapp.com/movie')
            .then(response => response.json())
            .then(movies => this.setState({ items: movies.results }))

    }

    onMenuClick = e => { 
        var path = '/';
        console.log(e.key)
        if (e.key != 'home'){
            path = `/${e.key}`
        }
    this.props.history.replace(path);
    }

    render() {
        const item = this.state.itemMovie;
        return (
            <div>
                {this.state.items.length > 0 ? (           //short if
                    <div style={{ height: '100' }}>
                        {' '}
                        <Layout className="Laout" style={{ background: 'thistle' }}>
                            <Header
                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%'
                                }}
                            >
                                <Menu
                                    theme="dark"
                                    mode="horizontal"
                                    defaultOpenKeys={[this.state.pathName]}
                                    style={{ lineHeight: '64px' }}
                                    onClick={e => {
                                         this.onMenuClick(e);
                                    }}
                                >
                                    <Menu.Item key={menus[0]}>Home</Menu.Item>
                                    <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                                    <Menu.Item key={menus[2]}>Profile</Menu.Item>
                                </Menu>
                            </Header>
                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    display: 'flex'
                                }}
                            >
                                <RouteManu 
                        items={this.state.items}
                        onItemMovieClick={this.onItemMovieClick}
                    />
                            </Content>
                            <Footer style={{ textAlign: 'center', background: 'thistle' }}>
                                Movie Application workshop @ CAMT
                </Footer>
                        </Layout>
                    </div>
                ) : (
                        <Spin size="large" />
                    )}

                {item != null ? (
                    <Modal
                        width="40%"
                        style={{ maxHeight: '70%' }}
                        title={item.title}
                        visible={this.state.isShowModel}
                        onCancel={this.onclickCancel}
                        footer={[
                            <Button
                                key="submit"
                                type="primary"
                                icon="heart"
                                size="large"
                                shape="circle"
                                onClick={this.heandlefavorite}
                            />,
                            <Button
                                key="submit"
                                type="primary"
                                icon="shopping-cart"
                                size="large"
                                shape="circle"
                                onClick={this.onclickBuyTicket}
                            />
                        ]}
                    >

                        <img src={item.image_url} style={{ width: '100%' }} />
                        <br />
                        <br />
                        <p>{item.overview}</p>
                    </Modal>
                ) : (
                        <div />
                    )}
            </div>
        )
    }
}
export default Main

